import openerplib

class Order():
    def create_order(self, connection):
        partner_model = connection.get_model('res.partner')
        so_model = connection.get_model('sale.order')
        product_model = connection.get_model('product.product')
        
        partner_ids = partner_model.search([('name', 'ilike', 'fletcher')])
        product_ids = product_model.search([('name', 'ilike', 'ipad')])
        
        order_id = so_model.create({
            'partner_id': partner_ids[0],
            'order_line': [(0,0,{'product_id': product_ids[0], 
                                 'product_uom_qty':1}),
                           (0,0,{'product_id': product_ids[1], 
                                 'product_uom_qty':2}),
                          ],
            
        })
        so_model.action_button_confirm([order_id,])
        

if __name__ == '__main__':
    #Connect by xml-rpc
    connection = openerplib.get_connection(hostname="localhost", 
                                           port=8069, 
                                           database="xp2015", 
                                           login="admin", 
                                           password="admin", 
                                           protocol="jsonrpc",
                                           user_id=1)
                                           
    connection.check_login()
            
    imp = Order()
    imp.create_order(connection)
