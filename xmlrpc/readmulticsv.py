import openerplib

class Products():
    def read_using_csv(self, connection):
        slicing = 4
        product_model = connection.get_model('product.product')
        
        product_ids = product_model.search([])
        
        product_list = []
        # To avoid the "CPU time limit exceeded.",
        # we slice the records to export in blocks
        begin=0
        while begin<len(product_ids):
            products = product_model.export_data(product_ids[begin:(begin+slicing)], 
                ['id', 'name', 'list_price'])
            product_list += products['datas']
            begin+=slicing
            
        print product_list

if __name__ == '__main__':
    #Connect by xml-rpc
    connection = openerplib.get_connection(hostname="localhost", 
                                           port=8069, 
                                           database="xp2015", 
                                           login="admin", 
                                           password="admin", 
                                           protocol="jsonrpc",
                                           user_id=1)
                                           
    connection.check_login()
            
    prod = Products()
    prod.read_using_csv(connection)
