import functools
import xmlrpclib

#Connection
url = 'http://127.0.0.1:8069'
db = 'xp2015'
username = 'admin'
password = 'admin'

common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
common.version()

#Logging in
uid = common.authenticate(db, username, password, {})

#Calling methods
models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(url))
print models.execute_kw(db, uid, password,
    'res.partner', 'check_access_rights',
    ['read'], {'raise_exception': False})

#List records
partner_ids = models.execute_kw(db, uid, password,
    'res.partner', 'search',
    [[['is_company', '=', True], ['customer', '=', True]]])
    
print partner_ids
