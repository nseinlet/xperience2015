import openerplib
import threading
import copy


class Products():
    max_connections = 3
    semaphore = threading.BoundedSemaphore(max_connections)
    lst_thd = []
    
    def _write_csv(self, model, columns, lines):
        sem = self.semaphore
        sem.acquire()
        try:
            model.load(columns, lines)
        finally:
            sem.release()
            
    def write_multi_csv(self, connection):
        slicing = 400
        product_model = connection.get_model('product.product')
        
        product_ids = product_model.search([])
        
        columns = ['id', 'list_price']
        lines = []
        
        products = product_model.export_data(product_ids, ['id', 'name', 'list_price'])
        for product in products['datas']:
            lines.append([product[0], float(product[2])*1.1])
            if len(lines)>=slicing:
                thd = threading.Thread(target=self._write_csv, args=(product_model, columns, copy.deepcopy(lines)))
                thd.start()
                self.lst_thd.append(thd)
                lines=[]
        if lines:
            thd = threading.Thread(target=self._write_csv, args=(product_model, columns, lines))
            thd.start()
            self.lst_thd.append(thd)
            
        for thd in self.lst_thd:
            thd.join()


if __name__ == '__main__':
    #Connect by xml-rpc
    connection = openerplib.get_connection(hostname="localhost", 
                                           port=8069, 
                                           database="xp2015", 
                                           login="admin", 
                                           password="admin", 
                                           protocol="jsonrpc",
                                           user_id=1)
                                           
    connection.check_login()
            
    prod = Products()
    prod.write_multi_csv(connection)
