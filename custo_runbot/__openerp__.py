# -*- coding: utf-8 -*-
{
    'name': "custo_runbot",

    'summary': """
        Small custos for runbot""",

    'description': """
        Small custos for the runbot
    """,

    'author': "Odoo s.a.",
    'website': "http://odoo.com",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['runbot'],

    'data': [
        'views/repo.xml',
    ],
}
